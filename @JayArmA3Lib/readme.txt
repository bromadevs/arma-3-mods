
JayArma2Extension
Jaynus' ArmA2 Extension Library
VERSION: 1.4.0

-------------------------------------------------------
INTRODUCTION
-------------------------------------------------------
JayArma2Lib is my creation to have access to either more efficient or extended functionality which the current ArmA2 engine lacks. This includes string functionality, data structure management, and named pipes access.

There are no requirements, except for using the latest  version of ArmA2 OA (at least past Beta 87662). Additionally, this library works BOTH Windows server and client side. 

All function calls remain the same as JayArmA2Lib, this just uses the functionality provided by BIS to communicate to outside programs via callExtension

-------------------------------------------------------
FEATURES
-------------------------------------------------------
- Named pipes access
- String manipulation functionality
- Hashtable (sorted list) functionality
- Logging to an external file
- Get local system time



-------------------------------------------------------
INSTALLATION
-------------------------------------------------------
You can follow an easy 1 step process for installation.

1. Copy all files to your ArmA2 Directory

2. Make sure you have JayArma2Extension.dll in the loaded folder (but not in the addons folder inside of it, it should be in the same directory as the addons folder).

-------------------------------------------------------
UNINSTALL
-------------------------------------------------------
Delete dsound.dll and all associated JayArma2Lib Files.


-------------------------------------------------------
FUNCTION INDEX
-------------------------------------------------------

STRING FUNCTIONS
	jayarma2lib_fnc_stringContains
		Function:
			BOOL ret = [String, StringToFind] call jayarma2lib_fnc_stringContains;
		Description:
			Checks to see if the first supplied string contains the second supplied string.
		Example:	
			_ret = ["foobar", "foo"] call jayarma2lib_fnc_stringContains;
			if(_ret == true) then  {
				...
			};
	
	jayarma2lib_fnc_stringIContains
		Function:
			BOOL ret = [String, StringToFind] call jayarma2lib_fnc_stringIContains;
	 	Description:
			Same as jayarma2lib_fnc_stringContains, except case insensitive.
		Example:
			_ret = ["foobar", "foo"] call jayarma2lib_fnc_stringIContains;
			if(_ret == true) then  {
				...
			};
			
	jayarma2lib_fnc_stringIndexOf	
		Function:
			INT position = [String, StringToFind] call jayarma2lib_fnc_stringIndexOf;
		Description:
			Returns the 0-based index of the requested string to search for. 
		Example:
			_pos = ["foobar", "bar"] call jayarma2lib_fnc_stringIndexOf;
			// _pos will be 3

			
	jayarma2lib_fnc_stringSubstr
		Function:
			STRING ret = [String, INT StartPosition, INT Length] call jayarma2lib_fnc_stringSubstr;
		Description:
			Returns the sub string from 0-based StartPosition, and length characters.
		Example:
			_sub = ["foobar", 3,3] call jayarma2lib_fnc_stringSubstr;
			// _sub will be "bar"
	
	jayarma2lib_fnc_stringTrim
		Function:
			STRING ret = [String] call jayarma2lib_fnc_stringTrim;
		Description:
			Removes all leading and trailing whitespaces from the provided string. 
		Example:
			_clean = ["   asdf    "] call jayarma2lib_fnc_stringTrim;
			// _clean will be "asdf"

NAMED PIPE FUNCTIONS
	jayarma2lib_fnc_openPipe
		Function:
			HANDLE pipeHandle = [PipePath] call jayarma2lib_fnc_openpipe;
		Description:
			Attempts to open the specific pipe name. If fail, returns nil. Otherwise, it returns a handle to the pipe.
		Example:
			_pipeHandle = ["\\.\pipe\jay] call jayarma2lib_fnc_openpipe;
			if(!isNil("_pipeHandle")) then {
				....
			};
			
	jayarma2lib_fnc_closePipe	
		Function:
			[HANDLE] call jayarma2lib_fnc_closepipe;
		Description:
			Attempts to close the provided pipe handle. fails silently; no return.
		Example:
			[_pipeHandle] call jayarma2lib_fnc_closepipe;
		
	jayarma2lib_fnc_readPipe
		Function:
			STRING ret = [HANDLE] call jayarma2lib_fnc_readpipe;
		Description:
			Reads any data which may be waiting on the pipe. If no data, returns nil
		Example:
			_data = [_pipeHandle] call jayarma2lib_fnc_readpipe;
			if(!isNil("_data")) then {
				....
			};
			
	jayarma2lib_fnc_writePipe
		Function:
			[HANDLE, StringToSend] call jayarma2lib_fnc_writepipe;
		Description:
			Attempts to write the provided data to the specified pipe handle. returns false 
			on failure, true on success.
		Example:
			_ret = [_pipeHandle, "Hello World!"] call jayarma2lib_fnc_writepipe;
			if(_ret == false) then {
				player sidechat "Write Failed!";
			};
		
		
HASHMAP FUNCTIONS
	jayarma2lib_fnc_hashmapCreate
		Function:
			BOOL ret = [StringHashmapName] call jayarma2lib_fnc_hashmapCreate;
		Description:
			Creates a new internally stored, thread safe hash map with the specified name. returns FALSE on failure, true on success.
		Example:
			["MyHashTable"] call jayarma2lib_fnc_hashmapCreate;
			
		
	jayarma2lib_fnc_hashmapDelete
		Function:
			BOOL ret = [StringHashmapName] call jayarma2lib_fnc_hashmapDelete;
		Description:
			Deletes and unallocates the specified hash map. returns FALSE on failure, TRUE on success.
		Example:
			["MyHashTable"] call jayarma2lib_fnc_hashmapDelete;
			
	jayarma2lib_fnc_hashmapDeleteValue	
		Function:
			BOOL ret = [StringHashmapName, StringKey] call jayarma2lib_fnc_hashmapDeleteValue;
		Description:
			Attempts to delete the specified key from the specified hash table.  returns FALSE on failure, TRUE on success.
		Example:
			_ret = ["MyHashTable", "MyKey"] call jayarma2lib_fnc_hashmapDeleteValue;
			if(_ret == false) then {
				// that key didnt exist...
			};
		
	jayarma2lib_fnc_hashmapAddValue
		Function: 
			BOOL ret = [StringHashmapName, StringKey, StringValue] call jayarma2lib_fnc_hashmapAddValue;
		Description:
			Attempts to add the provided key/value pair to the specified hash map. returns FALSE on failure, TRUE on success.
		Example:
			_ret = ["MyHashTable", "MyKey", "MyValue"] call jayarma2lib_fnc_hashmapAddValue;
			if(_ret == false) then {
				// that hash table didnt exist...
			};
		
	jayarma2lib_fnc_hashmapGetValue
		Function:
			STRING ret = [StringHashmapName, StringKey] call jayarma2lib_fnc_hashmapGetValue;
		Description:
			Attempts to get the value corrosponding to the provided key within the specified map. Returns the value on success, returns nil on failure.
		Example:
			_value = ["MyHashTable", "MyKey"] call jayarma2lib_fnc_hashmapGetValue;
			if(!isNil("_value")) then {
				....
			};


MISC FUNCTIONS
	
	jayarma2lib_fnc_writeLog
		Function:
			[StringToLog] call jayarma2lib_fnc_writelog;
		Description:
			Writes to the local JayArma2Lib.log located in the ArmA2 Root.
		Example:
			["Hello log world!"] call jayarma2lib_fnc_writelog;
			
	jayarma2lib_fnc_getLocalTime
		Function:
			INT ret = [] call jayarma2lib_fnc_getlocaltime;
		Description:
			Gets the local time of the system in which the command is ran. The time is provided in 24-Hour Metric Time (12.5 = 12:30).
		Example:
			_time = [] call jayarma2lib_fnc_getlocaltime;
			if(_time == 1.0) then {
				// its 1AM...
			};
			
			
-------------------------------------------------------
LINKS
-------------------------------------------------------
Dev-Heaven: http://dev-heaven.net/projects/jayarma2lib
BIS Forums: http://forums.bistudio.com/showthread.php?t=98647


-------------------------------------------------------
CREDITS
-------------------------------------------------------
- A2TS3 Team for helping with functionality requests
- Nou for addon help and getting me in touch with A2TS3
- Task Force Proteus @ TacticalGamer.com for being my
  guinea pigs.
- TacticalGamer.Com Admin team for supporting me and 
  helping me with so much server side testing (well, maybe not anymore)
- UO for testing (aka DO IT NOW!!!!!!)
 
 

-------------------------------------------------------
CONTACT
-------------------------------------------------------
Jaynus
E-Mail: walter.pearce@idi-systems.com
BIS Forums: jaynus
UnitedOperations.net Forums: jaynus

Nou
E-Mail: cliff.foster@idi-systems.com
BIS Forums: NouberNou
UnitedOperations.net Forums: Nou


-------------------------------------------------------
KNOWN BUGS
-------------------------------------------------------
