// AUTHOR	:	Terox (terox_@hotmail.com) www.zeus-community.net
// LAST Edited	:	7/4/2013
// MP MACHINE	:	Serverside	
// Called from	:	Zeu_ServerBriefing/init.sqf		
// Called using	: 	[]execVm
// Time Called	:	Before Time 0 
// Description	:	Defines AI skills 
//					(Temporary or permanent fix for the inability to define your precision and skill settings in the .ArmA3profile)
//					It also gives you more control over the entire skill settings for AI
//
//	COMMENTS 		skill array elements as follows
//
//						select 0:	aimingspeed
//						select 1:	aimingaccuracy
//						select 2:	aimingshake
//						select 3:	spotdistance
//						select 4:	spottime
//						select 5:	commanding
//						select 6:	courage
//						select 7:	endurance
//						select 8:	reloadSpeed
//						select 9:	general
//
//						Value range is between 0 and 1
	

  _option = [0,1];		// 1st element selects the skill matrix option
						// 2nd element selects the randomisation matrix option
						// The randomisation is +/- from the core selected value
						// For example if you used the _option = [3,2] then the 0.45 core value will be randomised to a value between 0.35 and 0.55
								
  _mode		= 1;		// 0: Addon module is disabled
						// 1: serverside only
						// 2: Server & Clientside
						// 3: Single player mode
						
  _debug	= FALSE;	// This will output the skill settings to your server .rpt for debugging purposes

   
  
  // The following is the core skill matrix
    _skill = switch(_option select 0)do
    {
         case 0: {[ 0.7,   0.3,  0.3,  0.8,   0.8, 0.5,  0.8, 0.8,  0.8, 1];};
         case 1: {[  0.4,  0.25,  0.5,  1,   0.15, 1,  0.5, 0.6,  0.6, 1];};
         case 2: {[ 0.45,   0.3,  0.5,  1,   0.15, 1,  0.6, 0.6,  0.6, 1];};
         case 3: {[  0.5,  0.34,  0.5,  1,    0.2, 1,  0.6, 0.6,  0.6, 1];};
         case 4: {[  0.6,   0.4,  0.5,  1,    0.3, 1,  0.6, 0.7,  0.7, 1];};
         case 5: {[ 0.65,  0.45,  0.5,  1,   0.35, 1,  0.6, 0.7,  0.7, 1];};
         case 6: {[  0.6,   0.5,  0.5,  1,    0.4, 1,  0.7, 0.8,  0.8, 1];};
         case 7: {[    1,     1,    1,  1,      1, 1,    1,   1,    1, 1];};
	 default {[  0.5,  0.34,  0.5,  1,    0.2, 1,  0.6, 0.6,  0.6, 1];};
    };
	
	// The following is the randomisation skill matrix
  _rnd = switch(_option select 1)do
  {
	case 0:{[    0,    0,    0,    0,     0,    0,     0,    0,     0, 0	];}; // use this option if you do not want randomisation
	case 1:{[ 0.01, 0.01, 0.01, 0.01,  0.01, 0.01,  0.01, 0.01,  0.01, 0	];};
	case 2:{[ 0.03, 0.03, 0.03, 0.03,  0.03, 0.03,  0.03, 0.03,  0.03, 0	];};
	case 3:{[ 0.05, 0.05, 0.05, 0.05,  0.05, 0.05,  0.05, 0.05,  0.05, 0	];};
	case 4:{[ 0.08, 0.08, 0.08, 0.08,  0.08, 0.08,  0.08, 0.08,  0.08, 0	];};
	case 5:{[  0.2,  0.2,  0.2,  0.2,   0.2,  0.2,   0.2,  0.2,   0.2, 0	];};
	default{[ 0.05, 0.05, 0.05, 0.05,  0.05, 0.05,  0.05, 0.05,  0.05, 0	];};
  };

// return
[_skill,_rnd,_mode,_debug];
